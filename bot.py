# -*- coding: utf-8 -*-
import config
import telebot
from telebot import types
from telebot.types import LabeledPrice
from models import *
from flask import Flask, request
import os

db.drop_tables([Users])
db.create_tables([Users])

pay_btn_mess = 0
click_invoice_id_rel_for_man_lat, click_invoice_id_rel_for_man_cyr = 0, 0
click_invoice_id_rel_for_woman_lat, click_invoice_id_rel_for_woman_cyr = 0, 0
click_invoice_id_finance_lat, click_invoice_id_finance_cyr = 0, 0
click_invoice_id_health_lat, click_invoice_id_health_cyr = 0, 0
click_invoice_id_happy_lat, click_invoice_id_happy_cyr = 0, 0
click_invoice_id_up_balance_lat, click_invoice_id_up_balance_cyr = 0, 0
payme_invoice_id_rel_for_man_lat, payme_invoice_id_rel_for_man_cyr = 0, 0
payme_invoice_id_rel_for_woman_lat, payme_invoice_id_rel_for_woman_cyr = 0, 0
payme_invoice_id_finance_lat, payme_invoice_id_finance_cyr = 0, 0
payme_invoice_id_health_lat, payme_invoice_id_health_cyr = 0, 0
payme_invoice_id_happy_lat, payme_invoice_id_happy_cyr = 0, 0
payme_invoice_id_up_balance_lat, payme_invoice_id_up_balance_cyr = 0, 0
isCardNumberInput = False
isSummVivod = False
card_number = 0
summ_to_vivod = 0

bot = telebot.TeleBot(config.token)
app = Flask(__name__)

ref_link = 'https://telegram.me/{}?start={}'
relations_price = [LabeledPrice(label='Отношения', amount=5000000)]
finance_price = [LabeledPrice(label='Финансы', amount=5000000)]
health_price = [LabeledPrice(label='Здоровье', amount=5000000)]
happy_price = [LabeledPrice(label='Счастье', amount=5000000)]
upbalance_price = [LabeledPrice(label='Пополнение баланса', amount=500000)]

PRODUCT_PRICE = 50000

# ВЫБОР ЛАТИНИЦЫ ИЛИ КИРИЛИЦЫ
keyboard_choose_lang = types.InlineKeyboardMarkup()
btn_lang_latinic = types.InlineKeyboardButton(text='🇺🇿 Lotincha 🇺🇿', callback_data="choosed_latinic")
btn_lang_cyrillic = types.InlineKeyboardButton(text='🇺🇿 Кириллча 🇺🇿', callback_data="choosed_cyrillic")
keyboard_choose_lang.row(btn_lang_latinic, btn_lang_cyrillic)

# ГЛАВНОЕ МЕНЮ
keyboard_main_menu_lat = types.InlineKeyboardMarkup()
btn_products = types.InlineKeyboardButton(text='🎁 Mahsulotlar 🎁', callback_data="products_lat")
btn_statistics = types.InlineKeyboardButton(text='📖 Stastika 📖', callback_data="statistics_lat")
btn_balance = types.InlineKeyboardButton(text='💰 Balans 💰', callback_data="balance_lat")
btn_goals = types.InlineKeyboardButton(text='📌 Maqsad va rejalar 📌', callback_data="goals_lat")
btn_marketing = types.InlineKeyboardButton(text='👥 Marketing reja 👥', callback_data="marketing_lat")
btn_instruction = types.InlineKeyboardButton(text="📋 Qo'llanma 📋️", callback_data="instruction_lat")
btn_ref_link = types.InlineKeyboardButton(text='🔗 Ref ssilka 🔗', callback_data="ref_link_lat")
btn_feedback = types.InlineKeyboardButton(text='⁉️ Aloqa uchun ⁉️', url='https://t.me/upgrade_uz_admin_bot')
btn_change_lang = types.InlineKeyboardButton(text='🇺🇿 Tilni almashtirish 🇺🇿', callback_data="change_lang")
keyboard_main_menu_lat.row(btn_products, btn_statistics)
keyboard_main_menu_lat.row(btn_balance, btn_goals)
keyboard_main_menu_lat.row(btn_marketing, btn_instruction)
keyboard_main_menu_lat.row(btn_ref_link, btn_feedback)
keyboard_main_menu_lat.row(btn_change_lang)

keyboard_main_menu_cyr = types.InlineKeyboardMarkup()
btn_products = types.InlineKeyboardButton(text='🎁 Маҳсулотлар 🎁', callback_data="products_cyr")
btn_statistics = types.InlineKeyboardButton(text='📖 Статистика 📖', callback_data="statistics_cyr")
btn_balance = types.InlineKeyboardButton(text='💰 Баланс 💰', callback_data="balance_cyr")
btn_goals = types.InlineKeyboardButton(text='📌 Мақсад ва режалар 📌', callback_data="goals_cyr")
btn_marketing = types.InlineKeyboardButton(text='👥 Маркетинг режа 👥', callback_data="marketing_cyr")
btn_instruction = types.InlineKeyboardButton(text='📋️ Қўлланма 📋️', callback_data="instruction_cyr")
btn_ref_link = types.InlineKeyboardButton(text='🔗 Реф ссылка 🔗', callback_data="ref_link_cyr")
btn_feedback = types.InlineKeyboardButton(text='⁉️ Алоқа учун ⁉️', url='https://t.me/upgrade_uz_admin_bot')
btn_change_lang = types.InlineKeyboardButton(text='🇺🇿 Тилни алмаштириш 🇺🇿', callback_data="change_lang")
keyboard_main_menu_cyr.row(btn_products, btn_statistics)
keyboard_main_menu_cyr.row(btn_balance, btn_goals)
keyboard_main_menu_cyr.row(btn_marketing, btn_instruction)
keyboard_main_menu_cyr.row(btn_ref_link, btn_feedback)
keyboard_main_menu_cyr.row(btn_change_lang)

# ПРОДУКЦИЯ
keyboard_products_lat = types.InlineKeyboardMarkup()
btn_relations = types.InlineKeyboardButton(text='👫 Munosabatlar 👨‍👩‍👧‍👦', callback_data="relations_lat")
btn_finance = types.InlineKeyboardButton(text='💰 Pul va boylik 💰', callback_data="finance_lat")
btn_health = types.InlineKeyboardButton(text='💊 Salomatlik 💊', callback_data="health_lat")
btn_happy = types.InlineKeyboardButton(text='😊 Baxt va omad 🙂', callback_data="happy_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_products_lat.row(btn_relations, btn_finance)
keyboard_products_lat.row(btn_health, btn_happy)
keyboard_products_lat.row(btn_back_to_home)

keyboard_products_cyr = types.InlineKeyboardMarkup()
btn_relations = types.InlineKeyboardButton(text='👫 Муносабатлар 👨‍👩‍👧‍👦', callback_data="relations_cyr")
btn_finance = types.InlineKeyboardButton(text='💰 Пул ва бойлик 💰', callback_data="finance_cyr")
btn_health = types.InlineKeyboardButton(text='💊 Саломатлик 💊', callback_data="health_cyr")
btn_happy = types.InlineKeyboardButton(text='😊 Бахт ва омад 🙂', callback_data="happy_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_products_cyr.row(btn_relations, btn_finance)
keyboard_products_cyr.row(btn_health, btn_happy)
keyboard_products_cyr.row(btn_back_to_home)

# ОТНОШЕНИЯ
keyboard_relations_lat = types.InlineKeyboardMarkup()
btn_rel_for_man = types.InlineKeyboardButton(text='👨 Erkaklar uchun 👨', callback_data="rel_for_man_lat")
btn_rel_for_woman = types.InlineKeyboardButton(text='👩 Ayollar uchun 👩', callback_data="rel_for_woman_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_relations_lat.row(btn_rel_for_man, btn_rel_for_woman)
keyboard_relations_lat.row(btn_back_to_home)

keyboard_relations_cyr = types.InlineKeyboardMarkup()
btn_rel_for_man = types.InlineKeyboardButton(text='👨 Эркаклар учун 👨', callback_data="rel_for_man_cyr")
btn_rel_for_woman = types.InlineKeyboardButton(text='👩 Аёллар учун 👩', callback_data="rel_for_woman_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_relations_cyr.row(btn_rel_for_man, btn_rel_for_woman)
keyboard_relations_cyr.row(btn_back_to_home)

# ОТНОШЕНИЯ ДЛЯ ПАРНЕЙ
keyboard_relations_for_man_lat = types.InlineKeyboardMarkup()
# btn_inner_content = types.InlineKeyboardButton(text='📋 Tarkibi 📋', callback_data="rel_inner_content_for_man_lat")
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="rel_click_for_man_lat")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="rel_payme_for_man_lat")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Balansdan 💲️', callback_data="rel_from_balance_for_man_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
#keyboard_relations_for_man_lat.row(btn_inner_content)
keyboard_relations_for_man_lat.row(btn_click, btn_payme)
keyboard_relations_for_man_lat.row(btn_from_balance)
keyboard_relations_for_man_lat.row(btn_back_to_home)

keyboard_relations_for_man_cyr = types.InlineKeyboardMarkup()
#btn_inner_content = types.InlineKeyboardButton(text='📋 Таркиби 📋', callback_data="rel_inner_content_for_man_cyr")
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="rel_click_for_man_cyr")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="rel_payme_for_man_cyr")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Балансдан 💲️', callback_data="rel_from_balance_for_man_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
#keyboard_relations_for_man_cyr.row(btn_inner_content)
keyboard_relations_for_man_cyr.row(btn_click, btn_payme)
keyboard_relations_for_man_cyr.row(btn_from_balance)
keyboard_relations_for_man_cyr.row(btn_back_to_home)

# ОТНОШЕНИЯ ДЛЯ ДЕВУШЕК
keyboard_relations_for_woman_lat = types.InlineKeyboardMarkup()
# btn_inner_content = types.InlineKeyboardButton(text='📋 Tarkibi 📋', callback_data="rel_inner_content_for_woman_lat")
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="rel_click_for_woman_lat")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="rel_payme_for_woman_lat")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Balansdan 💲️', callback_data="rel_from_balance_for_woman_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
# keyboard_relations_for_woman_lat.row(btn_inner_content)
keyboard_relations_for_woman_lat.row(btn_click, btn_payme)
keyboard_relations_for_woman_lat.row(btn_from_balance)
keyboard_relations_for_woman_lat.row(btn_back_to_home)

keyboard_relations_for_woman_cyr = types.InlineKeyboardMarkup()
# btn_inner_content = types.InlineKeyboardButton(text='📋 Таркиби 📋', callback_data="rel_inner_content_for_woman_cyr")
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="rel_click_for_woman_cyr")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="rel_payme_for_woman_cyr")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Балансдан 💲️', callback_data="rel_from_balance_for_woman_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
# keyboard_relations_for_woman_cyr.row(btn_inner_content)
keyboard_relations_for_woman_cyr.row(btn_click, btn_payme)
keyboard_relations_for_woman_cyr.row(btn_from_balance)
keyboard_relations_for_woman_cyr.row(btn_back_to_home)

# ФИНАНСЫ
keyboard_finance_lat = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="finance_click_lat")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="finance_payme_lat")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Balansdan 💲️', callback_data="finance_from_balance_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_finance_lat.row(btn_click, btn_payme)
keyboard_finance_lat.row(btn_from_balance)
keyboard_finance_lat.row(btn_back_to_home)

keyboard_finance_cyr = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="finance_click_cyr")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="finance_payme_cyr")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Балансдан 💲️', callback_data="finance_from_balance_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_finance_cyr.row(btn_click, btn_payme)
keyboard_finance_cyr.row(btn_from_balance)
keyboard_finance_cyr.row(btn_back_to_home)

# ЗДОРОВЬЕ
keyboard_health_lat = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="health_click_lat")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="health_payme_lat")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Balansdan 💲️', callback_data="health_from_balance_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_health_lat.row(btn_click, btn_payme)
keyboard_health_lat.row(btn_from_balance)
keyboard_health_lat.row(btn_back_to_home)

keyboard_health_cyr = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="health_click_cyr")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="health_payme_cyr")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Балансдан 💲️', callback_data="health_from_balance_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_health_cyr.row(btn_click, btn_payme)
keyboard_health_cyr.row(btn_from_balance)
keyboard_health_cyr.row(btn_back_to_home)

# СЧАСТЬЕ И УСПЕХ
keyboard_happy_lat = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="happy_click_lat")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="happy_payme_lat")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Balansdan 💲️', callback_data="happy_from_balance_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_happy_lat.row(btn_click, btn_payme)
keyboard_happy_lat.row(btn_from_balance)
keyboard_happy_lat.row(btn_back_to_home)

keyboard_happy_cyr = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳', callback_data="happy_click_cyr")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="happy_payme_cyr")
btn_from_balance = types.InlineKeyboardButton(text='💲️ Балансдан 💲️', callback_data="happy_from_balance_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_happy_cyr.row(btn_click, btn_payme)
keyboard_happy_cyr.row(btn_from_balance)
keyboard_happy_cyr.row(btn_back_to_home)

# МАРКЕТИНГ ПЛАН
keyboard_marketing_lat = types.InlineKeyboardMarkup()
btn_marketing = types.InlineKeyboardButton(text="Marketing reja", callback_data="marketing_plan_lat")
btn_5days = types.InlineKeyboardButton(text="5 kunda 103 million so'm daromad etish", callback_data="5days_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_marketing_lat.row(btn_marketing, btn_5days)
keyboard_marketing_lat.row(btn_back_to_home)

keyboard_marketing_cyr = types.InlineKeyboardMarkup()
btn_marketing = types.InlineKeyboardButton(text="Маркетинг режа", callback_data="marketing_plan_cyr")
btn_5days = types.InlineKeyboardButton(text="5 кунда 103 миллион сўм даромад этиш", callback_data="5days_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_marketing_cyr.row(btn_marketing, btn_5days)
keyboard_marketing_cyr.row(btn_back_to_home)

# ИНСТРУКЦИЯ
keyboard_instruction_lat = types.InlineKeyboardMarkup()
btn_xarid_qilish = types.InlineKeyboardButton(text="Xarid qilish", callback_data="xarid_qilish_lat")
btn_pul_yechish = types.InlineKeyboardButton(text="Pul yechish", callback_data="pul_yechish_lat")
btn_qoidalar = types.InlineKeyboardButton(text="Tavsiyalar", callback_data="qoidalar_lat")
btn_100ta_sabab = types.InlineKeyboardButton(text="100 ta sabab", callback_data="100ta_sabab_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_instruction_lat.row(btn_xarid_qilish, btn_pul_yechish)
keyboard_instruction_lat.row(btn_qoidalar, btn_100ta_sabab)
keyboard_instruction_lat.row(btn_back_to_home)

keyboard_instruction_cyr = types.InlineKeyboardMarkup()
btn_xarid_qilish = types.InlineKeyboardButton(text="Харид қилиш", callback_data="xarid_qilish_cyr")
btn_pul_yechish = types.InlineKeyboardButton(text="Пул ечиш", callback_data="pul_yechish_cyr")
btn_qoidalar = types.InlineKeyboardButton(text="Тавсиялар", callback_data="qoidalar_cyr")
btn_100ta_sabab = types.InlineKeyboardButton(text="100 та сабаб", callback_data="100ta_sabab_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_instruction_cyr.row(btn_xarid_qilish, btn_pul_yechish)
keyboard_instruction_cyr.row(btn_qoidalar, btn_100ta_sabab)
keyboard_instruction_cyr.row(btn_back_to_home)

# БАЛАНС
keyboard_balance_lat = types.InlineKeyboardMarkup()
btn_up_balance = types.InlineKeyboardButton(text="Balansni to'ldirish", callback_data="up_balance_lat")
btn_vivod_sredstv = types.InlineKeyboardButton(text='Pulni chiqarish', callback_data="vivod_sredstv_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_balance_lat.row(btn_up_balance, btn_vivod_sredstv)
keyboard_balance_lat.row(btn_back_to_home)

keyboard_balance_cyr = types.InlineKeyboardMarkup()
btn_up_balance = types.InlineKeyboardButton(text="Балансни тўлдириш", callback_data="up_balance_cyr")
btn_vivod_sredstv = types.InlineKeyboardButton(text='Пулни чиқариш', callback_data="vivod_sredstv_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_balance_cyr.row(btn_up_balance, btn_vivod_sredstv)
keyboard_balance_cyr.row(btn_back_to_home)

# ПОПОЛНЕНИЕ БАЛАНСА
keyboard_up_balance_lat = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳 ', callback_data="up_balance_click_lat")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="up_balance_payme_lat")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_up_balance_lat.add(btn_click, btn_payme)
keyboard_up_balance_lat.row(btn_back_to_home)

keyboard_up_balance_cyr = types.InlineKeyboardMarkup()
btn_click = types.InlineKeyboardButton(text='💳 CLICK 💳 ', callback_data="up_balance_click_cyr")
btn_payme = types.InlineKeyboardButton(text='💳 PAYME 💳', callback_data="up_balance_payme_cyr")
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_cyr")
keyboard_up_balance_cyr.add(btn_click, btn_payme)
keyboard_up_balance_cyr.row(btn_back_to_home)

# КНОПКА В ГЛАВНОЕ МЕНЮ
keyboard_back_to_home_lat = types.InlineKeyboardMarkup()
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Bosh sahifaga qaytish ↪️', callback_data="back_to_home_lat")
keyboard_back_to_home_lat.row(btn_back_to_home)

keyboard_back_to_home_cyr = types.InlineKeyboardMarkup()
btn_back_to_home = types.InlineKeyboardButton(text='↪️ Бош саҳифага қайтиш ↪️', callback_data="back_to_home_cyr")
keyboard_back_to_home_cyr.row(btn_back_to_home)

# КНОПКА ОТМЕНЫ
keyboard_cancel_rel_for_man_lat = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌ Bekor qilish ❌️', callback_data="cancel_rel_for_man_lat")
keyboard_cancel_rel_for_man_lat.row(btn_cancel)
keyboard_cancel_rel_for_man_cyr = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌️ Бекор килиш ❌️', callback_data="cancel_rel_for_man_cyr")
keyboard_cancel_rel_for_man_cyr.row(btn_cancel)

keyboard_cancel_rel_for_woman_lat = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌ Bekor qilish ❌️', callback_data="cancel_rel_for_woman_lat")
keyboard_cancel_rel_for_woman_lat.row(btn_cancel)
keyboard_cancel_rel_for_woman_cyr = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌️ Бекор килиш ❌️', callback_data="cancel_rel_for_woman_cyr")
keyboard_cancel_rel_for_woman_cyr.row(btn_cancel)

keyboard_cancel_finance_lat = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌ Bekor qilish ❌️', callback_data="cancel_finance_lat")
keyboard_cancel_finance_lat.row(btn_cancel)
keyboard_cancel_finance_cyr = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌️ Бекор килиш ❌️', callback_data="cancel_finance_cyr")
keyboard_cancel_finance_cyr.row(btn_cancel)

keyboard_cancel_health_lat = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌ Bekor qilish ❌️', callback_data="cancel_health_lat")
keyboard_cancel_health_lat.row(btn_cancel)
keyboard_cancel_health_cyr = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌️ Бекор килиш ❌️', callback_data="cancel_health_cyr")
keyboard_cancel_health_cyr.row(btn_cancel)

keyboard_cancel_happy_lat = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌ Bekor qilish ❌️', callback_data="cancel_happy_lat")
keyboard_cancel_happy_lat.row(btn_cancel)
keyboard_cancel_happy_cyr = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌️ Бекор килиш ❌️', callback_data="cancel_happy_cyr")
keyboard_cancel_happy_cyr.row(btn_cancel)

keyboard_cancel_up_balance_lat = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌ Bekor qilish ❌️', callback_data="cancel_up_balance_lat")
keyboard_cancel_up_balance_lat.row(btn_cancel)
keyboard_cancel_up_balance_cyr = types.InlineKeyboardMarkup()
btn_cancel = types.InlineKeyboardButton(text='❌️ Бекор килиш ❌️', callback_data="cancel_up_balance_cyr")
keyboard_cancel_up_balance_cyr.row(btn_cancel)

keyboard_buy_from_balance_relations = types.InlineKeyboardMarkup()  # RELATIONS FROM BALANCE KEYBOARD
btn_buy_from_balance = types.InlineKeyboardButton(text=f'Заплатить {PRODUCT_PRICE} UZS', callback_data="buy_from_balance_relations")
keyboard_buy_from_balance_relations.add(btn_buy_from_balance)

keyboard_buy_from_balance_finance = types.InlineKeyboardMarkup()  # FINANCE FROM BALANCE KEYBOARD
btn_buy_from_balance = types.InlineKeyboardButton(text=f'Заплатить {PRODUCT_PRICE} UZS', callback_data="buy_from_balance_finance")
keyboard_buy_from_balance_finance.add(btn_buy_from_balance)

keyboard_buy_from_balance_health = types.InlineKeyboardMarkup()  # HEALTH FROM BALANCE KEYBOARD
btn_buy_from_balance = types.InlineKeyboardButton(text=f'Заплатить {PRODUCT_PRICE} UZS', callback_data="buy_from_balance_health")
keyboard_buy_from_balance_health.add(btn_buy_from_balance)

keyboard_buy_from_balance_happy = types.InlineKeyboardMarkup()  # HAPPY FROM BALANCE KEYBOARD
btn_buy_from_balance = types.InlineKeyboardButton(text=f'Заплатить {PRODUCT_PRICE} UZS', callback_data="buy_from_balance_happy")
keyboard_buy_from_balance_happy.add(btn_buy_from_balance)

@bot.message_handler(commands=['start'])
def start(message):
    user_id = message.chat.id
    #vtest = bot.get_chat('@upgradeuz_savollar')
    #bot.send_message(message.chat.id, vtest)
    splited = message.text.split()
    bot.send_message(message.chat.id, 'Tilni tanglang / Тилни танланг',
                     reply_markup=keyboard_choose_lang)
    #bot.send_message(message.chat.id, bot.get_chat('@Shaxzod_and_Ko'))
    if not Users.user_exists(user_id):  # Если пользователь не был зареган
        if len(splited) == 2:  # Если тебя пригласили
            parent_id = splited[1]
            r_id_2 = Users.get(user_id=parent_id).ref_id_1
            r_id_3 = Users.get(user_id=parent_id).ref_id_2
            r_id_4 = Users.get(user_id=parent_id).ref_id_3
            r_id_5 = Users.get(user_id=parent_id).ref_id_4
            Users.create(user_id=user_id, ref_id_1=parent_id, ref_id_2=r_id_2, ref_id_3=r_id_3,
                         ref_id_4=r_id_4, ref_id_5=r_id_5)
            # bot.send_message(message.chat.id, f'Вас пригласил: {parent_id}', reply_markup=keyboard_choose_lang)
        else:  # Если ты зашёл сам
            Users.create(user_id=user_id)
            # bot.send_message(message.chat.id, 'Вы зарегестрировались!', reply_markup=keyboard_choose_lang)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    # Если сообщение из чата с ботом
    if call.message:
        if call.data == "choosed_latinic":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/OZBEKISTON-XALQIGA-MUROJAAT-12-07", reply_markup=keyboard_main_menu_lat)
        if call.data == "choosed_cyrillic":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/%D0%8EZBEKISTON-HAL%D2%9AIGA-MUROZHAAT-12-07", reply_markup=keyboard_main_menu_cyr)
        if call.data == "change_lang":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Tilni tanglang / Тилни танланг", reply_markup=keyboard_choose_lang)
        if call.data == "back_to_home_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/OZBEKISTON-XALQIGA-MUROJAAT-12-07", reply_markup=keyboard_main_menu_lat)
        if call.data == "back_to_home_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/%D0%8EZBEKISTON-HAL%D2%9AIGA-MUROZHAAT-12-07", reply_markup=keyboard_main_menu_cyr)
        if call.data == "products_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Bo'limni tanlang:", reply_markup=keyboard_products_lat)
        if call.data == "products_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Бўлимни танланг:", reply_markup=keyboard_products_cyr)
        if call.data == "relations_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Bo'limni tanlang:", reply_markup=keyboard_relations_lat)
        if call.data == "relations_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Бўлимни танланг:", reply_markup=keyboard_relations_cyr)

        if call.data == "rel_for_man_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/MUNOSABATLAR-BOLIMI-HAQIDA-12-09", reply_markup=keyboard_relations_for_man_lat)
        if call.data == "rel_for_man_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/MUNOSABATLAR-B%D0%8ELIMI-%D2%B2A%D2%9AIDA-12-09", reply_markup=keyboard_relations_for_man_cyr)
        if call.data == "rel_for_woman_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/MUNOSABATLAR-BOLIMI-HAQIDA-12-09-2", reply_markup=keyboard_relations_for_woman_lat)
        if call.data == "rel_for_woman_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/MUNOSABATLAR-B%D0%8ELIMI-%D2%B2A%D2%9AIDA-12-09-2", reply_markup=keyboard_relations_for_woman_cyr)
        if call.data == "finance_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/PUL-VA-BOYLIK-BOLIMI-HAQIDA-12-09",
                                  reply_markup=keyboard_finance_lat)
        if call.data == "finance_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/PUL-VA-BOJLIK-B%D0%8ELIMI-%D2%B2A%D2%9AIDA-12-09",
                                  reply_markup=keyboard_finance_cyr)
        if call.data == "health_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/SALOMATLIK-BOLIMI-HAQIDA-12-09",
                                  reply_markup=keyboard_health_lat)
        if call.data == "health_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/SALOMATLIK-B%D0%8ELIMI-%D2%B2A%D2%9AIDA-12-09",
                                  reply_markup=keyboard_health_cyr)
        if call.data == "happy_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/BAXT-VA-OMAD-IZLAYOTGANLAR-UCHUN-BOLIMI-HAQIDA-12-09",
                                  reply_markup=keyboard_happy_lat)
        if call.data == "happy_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/BAHT-VA-OMAD-IZLAYOTGANLAR-UCHUN-B%D0%8ELIMI-%D2%B2A%D2%9AIDA-12-09",
                                  reply_markup=keyboard_happy_cyr)

        if call.data == "rel_click_for_man_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_rel_for_man_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Munosabatlar erkaklar uchun',
                             description='Munosabatlar erkaklar uchun',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_man_lat')
            click_invoice_id_rel_for_man_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_man_lat)
        if call.data == "rel_click_for_man_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_rel_for_man_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Муносабатлар эркаклар учун',
                             description='Муносабатлар эркаклар учун',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_man_cyr')
            click_invoice_id_rel_for_man_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_man_cyr)
        if call.data == "rel_click_for_woman_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_rel_for_woman_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Munosabatlar ayollar uchun',
                             description='Munosabatlar ayollar uchun',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_woman_lat')
            click_invoice_id_rel_for_woman_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_woman_lat)
        if call.data == "rel_click_for_woman_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_rel_for_woman_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Муносабатлар аёллар учун',
                             description='Муносабатлар аёллар учун',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_woman_cyr')
            click_invoice_id_rel_for_woman_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_woman_cyr)
        if call.data == "finance_click_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_finance_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Pul va boylik',
                             description='Pul va boylik',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=finance_price,
                             start_parameter='time-machine-example',
                             invoice_payload='finance_lat')
            click_invoice_id_finance_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_finance_lat)
        if call.data == "finance_click_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_finance_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Пул ва бойлик',
                             description='Пул ва бойлик',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=finance_price,
                             start_parameter='time-machine-example',
                             invoice_payload='finance_cyr')
            click_invoice_id_finance_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_finance_cyr)
        if call.data == "health_click_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_health_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Salomatlik',
                             description='Salomatlik',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=health_price,
                             start_parameter='time-machine-example',
                             invoice_payload='health_lat')
            click_invoice_id_health_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_health_lat)
        if call.data == "health_click_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_health_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Саломатлик',
                             description='Саломатлик',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=health_price,
                             start_parameter='time-machine-example',
                             invoice_payload='health_cyr')
            click_invoice_id_health_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_health_cyr)
        if call.data == "happy_click_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_happy_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Baxt va omad',
                             description='Baxt va omad',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=happy_price,
                             start_parameter='time-machine-example',
                             invoice_payload='happy_lat')
            click_invoice_id_happy_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_happy_lat)
        if call.data == "happy_click_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_happy_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Бахт ва омад',
                             description='Бахт ва омад',
                             provider_token=config.click_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=happy_price,
                             start_parameter='time-machine-example',
                             invoice_payload='happy_cyr')
            click_invoice_id_happy_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_happy_cyr)

        if call.data == "rel_payme_for_man_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_rel_for_man_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Munosabatlar erkaklar uchun',
                             description='Munosabatlar erkaklar uchun',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_man_lat')
            payme_invoice_id_rel_for_man_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_man_lat)
        if call.data == "rel_payme_for_man_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_rel_for_man_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Муносабатлар эркаклар учун',
                             description='Муносабатлар эркаклар учун',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_man_cyr')
            payme_invoice_id_rel_for_man_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_man_cyr)
        if call.data == "rel_payme_for_woman_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_rel_for_woman_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Munosabatlar ayollar uchun',
                             description='Munosabatlar ayollar uchun',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_woman_lat')
            payme_invoice_id_rel_for_woman_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_woman_lat)
        if call.data == "rel_payme_for_woman_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_rel_for_woman_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Муносабатлар аёллар учун',
                             description='Муносабатлар аёллар учун',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=relations_price,
                             start_parameter='time-machine-example',
                             invoice_payload='rel_for_woman_cyr')
            payme_invoice_id_rel_for_woman_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_rel_for_woman_cyr)
        if call.data == "finance_payme_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_finance_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Pul va boylik',
                             description='Pul va boylik',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=finance_price,
                             start_parameter='time-machine-example',
                             invoice_payload='finance_lat')
            payme_invoice_id_finance_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_finance_lat)
        if call.data == "finance_payme_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_finance_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Пул ва бойлик',
                             description='Пул ва бойлик',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=finance_price,
                             start_parameter='time-machine-example',
                             invoice_payload='finance_cyr')
            payme_invoice_id_finance_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_finance_cyr)
        if call.data == "health_payme_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_health_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Salomatlik',
                             description='Salomatlik',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=health_price,
                             start_parameter='time-machine-example',
                             invoice_payload='health_lat')
            payme_invoice_id_health_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_health_lat)
        if call.data == "health_payme_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_health_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Саломатлик',
                             description='Саломатлик',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=health_price,
                             start_parameter='time-machine-example',
                             invoice_payload='health_cyr')
            payme_invoice_id_health_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_health_cyr)
        if call.data == "happy_payme_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_happy_lat
            invoice = bot.send_invoice(call.message.chat.id, title='Baxt va omad',
                             description='Baxt va omad',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=happy_price,
                             start_parameter='time-machine-example',
                             invoice_payload='happy_lat')
            payme_invoice_id_happy_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_happy_lat)
        if call.data == "happy_payme_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_happy_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Бахт ва омад',
                             description='Бахт ва омад',
                             provider_token=config.payme_token,
                             currency='UZS',
                             is_flexible=False,
                             prices=happy_price,
                             start_parameter='time-machine-example',
                             invoice_payload='happy_cyr')
            payme_invoice_id_happy_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_happy_cyr)

        if call.data == "rel_from_balance_for_man_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Вы приобрели блок 'Munosabatlar yigitlar uchun'",
                                      reply_markup=keyboard_back_to_home_lat)
                file_data = open('files/munosabatlar_yigitlar_uchun_lat.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_lat,
                                      reply_markup=keyboard_back_to_home_lat)
        if call.data == "rel_from_balance_for_man_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Вы приобрели блок 'Munosabatlar yigitlar uchun'",
                                      reply_markup=keyboard_back_to_home_cyr)
                file_data = open('files/munosabatlar_yigitlar_uchun_cyr.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_cyr,
                                      reply_markup=keyboard_back_to_home_cyr)
        if call.data == "rel_from_balance_for_woman_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Вы приобрели блок 'Munosabatlar ayollar uchun'",
                                      reply_markup=keyboard_back_to_home_lat)
                file_data = open('files/munosabatlar_ayollar_uchun_lat.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_lat,
                                      reply_markup=keyboard_back_to_home_lat)
        if call.data == "rel_from_balance_for_woman_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Вы приобрели блок 'Munosabatlar ayollar uchun'",
                                      reply_markup=keyboard_back_to_home_cyr)
                file_data = open('files/munosabatlar_ayollar_uchun_cyr.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_cyr,
                                      reply_markup=keyboard_back_to_home_cyr)
        if call.data == "finance_from_balance_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                file_data = open('files/pul_va_boylik_lat.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_lat,
                                      reply_markup=keyboard_back_to_home_lat)
        if call.data == "finance_from_balance_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                file_data = open('files/pul_va_boylik_cyr.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_cyr,
                                      reply_markup=keyboard_back_to_home_cyr)
        if call.data == "health_from_balance_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                file_data = open('files/salomatlik_lat.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_lat,
                                      reply_markup=keyboard_back_to_home_lat)
        if call.data == "health_from_balance_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                file_data = open('files/salomatlik_cyr.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_cyr,
                                      reply_markup=keyboard_back_to_home_cyr)
        if call.data == "happy_from_balance_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                file_data = open('files/baxt_va_omad_lat.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_lat,
                                      reply_markup=keyboard_back_to_home_lat)
        if call.data == "happy_from_balance_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal > PRODUCT_PRICE:
                file_data = open('files/baxt_va_omad_cyr.zip', 'rb')
                bot.send_document(call.message.chat.id, file_data)
                send_percents(call.message)
                query = Users.update(balance=Users.balance - PRODUCT_PRICE).where(Users.user_id == call.message.chat.id)
                query.execute()
                query = Users.update(purchase_count=Users.purchase_count + 1).where(
                    Users.user_id == call.message.chat.id)
                query.execute()
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.nedostatochno_sredstv_cyr,
                                      reply_markup=keyboard_back_to_home_cyr)
        if call.data == "marketing_plan_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text=f'Sizning hisobingizda: {bal}', reply_markup=keyboard_balance_lat)
        if call.data == "balance_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text=f'Sizning hisobingizda: {bal}', reply_markup=keyboard_balance_lat)
        if call.data == "balance_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text=f'Сизнинг ҳисобингизда: {bal}', reply_markup=keyboard_balance_cyr)
        if call.data == "up_balance_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text=f'Sizning hisobingizda: {bal}', reply_markup=keyboard_up_balance_lat)
        if call.data == "up_balance_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text=f'Сизнинг ҳисобингизда: {bal}', reply_markup=keyboard_up_balance_cyr)
        if call.data == "up_balance_click_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_up_balance_lat
            invoice = bot.send_invoice(call.message.chat.id, title="Hisobni to'ldirish",
                                       description="Hisobni to'ldirish",
                                       provider_token=config.click_token,
                                       currency='UZS',
                                       is_flexible=False,
                                       prices=upbalance_price,
                                       start_parameter='time-machine-example',
                                       invoice_payload='upbalance')
            click_invoice_id_up_balance_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_up_balance_lat)
        if call.data == "up_balance_click_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global click_invoice_id_up_balance_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Ҳисобни тўлдириш',
                                       description='Ҳисобни тўлдириш',
                                       provider_token=config.click_token,
                                       currency='UZS',
                                       is_flexible=False,
                                       prices=upbalance_price,
                                       start_parameter='time-machine-example',
                                       invoice_payload='upbalance')
            click_invoice_id_up_balance_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_up_balance_cyr)
        if call.data == "up_balance_payme_lat":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_up_balance_lat
            invoice = bot.send_invoice(call.message.chat.id, title="Hisobni to'ldirish",
                                       description="Hisobni to'ldirish",
                                       provider_token=config.payme_token,
                                       currency='UZS',
                                       is_flexible=False,
                                       prices=upbalance_price,
                                       start_parameter='time-machine-example',
                                       invoice_payload='upbalance')
            payme_invoice_id_up_balance_lat = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_up_balance_lat)
        if call.data == "up_balance_payme_cyr":
            bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.message_id)
            global payme_invoice_id_up_balance_cyr
            invoice = bot.send_invoice(call.message.chat.id, title='Ҳисобни тўлдириш',
                                       description='Ҳисобни тўлдириш',
                                       provider_token=config.payme_token,
                                       currency='UZS',
                                       is_flexible=False,
                                       prices=upbalance_price,
                                       start_parameter='time-machine-example',
                                       invoice_payload='upbalance')
            payme_invoice_id_up_balance_cyr = invoice.message_id
            bot.send_message(call.message.chat.id, "-------------------️----------------------",
                             reply_markup=keyboard_cancel_up_balance_cyr)
        if call.data == "vivod_sredstv_lat":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal >= PRODUCT_PRICE:
                msg = bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Kartangiz nomerini kiriting.", reply_markup=keyboard_back_to_home_cyr)
                bot.register_next_step_handler(msg, send_for_vivod)
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Pul yechishning minimal miqdori 50.000 so'm!", reply_markup=keyboard_back_to_home_cyr)
        if call.data == "vivod_sredstv_cyr":
            bal = Users.get(user_id=call.message.chat.id).balance
            if bal >= PRODUCT_PRICE:
                msg = bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Картангиз номерини киритинг.", reply_markup=keyboard_back_to_home_cyr)
                bot.register_next_step_handler(msg, send_for_vivod_cyr)
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Пул ечишнинг минимал миқдори 50.000 сўм!", reply_markup=keyboard_back_to_home_cyr)
        if call.data == "feedback_lat":
            msg = bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                        text="Savol, takliflar va olgan natijalaringiz haqida yozib qoldiring. Administrator sizga tez orada javob beradi.", reply_markup=keyboard_back_to_home_lat)
            bot.register_next_step_handler(msg, send_for_question)
        if call.data == "feedback_cyr":
            msg = bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                        text="Савол, таклифлар ва олган натижаларингиз ҳакида ёзиб колдиринг. Администратор сизга тез орада жавоб беради.", reply_markup=keyboard_back_to_home_cyr)
            bot.register_next_step_handler(msg, send_for_question)


        if call.data == "cancel_rel_for_man_lat":
            if click_invoice_id_rel_for_man_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_rel_for_man_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                click_invoice_id_rel_for_man_lat = 0
            if payme_invoice_id_rel_for_man_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_rel_for_man_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                payme_invoice_id_rel_for_man_lat = 0
        if call.data == "cancel_rel_for_man_cyr":
            if click_invoice_id_rel_for_man_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_rel_for_man_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                click_invoice_id_rel_for_man_cyr = 0
            if payme_invoice_id_rel_for_man_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_rel_for_man_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                payme_invoice_id_rel_for_man_cyr = 0
        if call.data == "cancel_rel_for_woman_lat":
            if click_invoice_id_rel_for_woman_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_rel_for_woman_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                click_invoice_id_rel_for_woman_lat = 0
            if payme_invoice_id_rel_for_woman_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_rel_for_woman_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                payme_invoice_id_rel_for_woman_lat = 0
        if call.data == "cancel_rel_for_woman_cyr":
            if click_invoice_id_rel_for_woman_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_rel_for_woman_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                click_invoice_id_rel_for_woman_cyr = 0
            if payme_invoice_id_rel_for_woman_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_rel_for_woman_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                payme_invoice_id_rel_for_woman_cyr = 0
        if call.data == "cancel_finance_lat":
            if click_invoice_id_finance_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_finance_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                click_invoice_id_finance_lat = 0
            if payme_invoice_id_finance_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_finance_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                payme_invoice_id_finance_lat = 0
        if call.data == "cancel_finance_cyr":
            if click_invoice_id_finance_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_finance_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                click_invoice_id_finance_cyr = 0
            if payme_invoice_id_finance_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_finance_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                payme_invoice_id_finance_cyr = 0
        if call.data == "cancel_health_lat":
            if click_invoice_id_health_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_health_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                click_invoice_id_health_lat = 0
            if payme_invoice_id_health_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_health_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                payme_invoice_id_health_lat = 0
        if call.data == "cancel_health_cyr":
            if click_invoice_id_health_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_health_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                click_invoice_id_health_cyr = 0
            if payme_invoice_id_health_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_health_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                payme_invoice_id_health_cyr = 0
        if call.data == "cancel_happy_lat":
            if click_invoice_id_happy_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_happy_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                click_invoice_id_happy_lat = 0
            if payme_invoice_id_happy_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_happy_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                payme_invoice_id_happy_lat = 0
        if call.data == "cancel_happy_cyr":
            if click_invoice_id_happy_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_happy_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                click_invoice_id_happy_cyr = 0
            if payme_invoice_id_happy_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_happy_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                payme_invoice_id_happy_cyr = 0
        if call.data == "cancel_up_balance_lat":
            if click_invoice_id_up_balance_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_up_balance_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                click_invoice_id_up_balance_lat = 0
            if payme_invoice_id_up_balance_lat > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_up_balance_lat)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_lat,
                                      reply_markup=keyboard_main_menu_lat)
                payme_invoice_id_up_balance_lat = 0
        if call.data == "cancel_up_balance_cyr":
            if click_invoice_id_up_balance_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=click_invoice_id_up_balance_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                click_invoice_id_up_balance_cyr = 0
            if payme_invoice_id_up_balance_cyr > 0:
                bot.delete_message(chat_id=call.message.chat.id, message_id=payme_invoice_id_up_balance_cyr)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=config.bosh_sahifa_cyr,
                                      reply_markup=keyboard_main_menu_cyr)
                payme_invoice_id_up_balance_cyr = 0







        if call.data == "instruction_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Bo'limni tanlang:", reply_markup=keyboard_instruction_lat)
        if call.data == "instruction_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Бўлимни танланг:", reply_markup=keyboard_instruction_cyr)
        if call.data == "xarid_qilish_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/BOLIMNI-XARID-QILISH-12-10", reply_markup=keyboard_back_to_home_lat)
        if call.data == "xarid_qilish_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/B%D0%8ELIMNI-HARID-%D2%9AILISH-12-10", reply_markup=keyboard_back_to_home_cyr)
        if call.data == "pul_yechish_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/DAROMADNI-PLASTIK-KARTAGA-CHIQARISH-12-10",
                                  reply_markup=keyboard_back_to_home_lat)
        if call.data == "pul_yechish_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/DAROMADNI-PLASTIK-KARTAGA-CHI%D2%9AARISH-12-10",
                                  reply_markup=keyboard_back_to_home_cyr)
        if call.data == "qoidalar_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/QOIDALAR-12-10",
                                  reply_markup=keyboard_back_to_home_lat)
        if call.data == "qoidalar_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/%D2%9AOIDALAR-12-10",
                                  reply_markup=keyboard_back_to_home_cyr)
        if call.data == "100ta_sabab_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/100-TA-SABAB-12-10",
                                  reply_markup=keyboard_back_to_home_lat)
        if call.data == "100ta_sabab_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/100-TA-SABAB-12-10-2",
                                  reply_markup=keyboard_back_to_home_cyr)







        if call.data == "marketing_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Bo'limni tanlang:", reply_markup=keyboard_marketing_lat)
        if call.data == "marketing_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Бўлимни танланг:", reply_markup=keyboard_marketing_cyr)
        if call.data == "marketing_plan_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/MARKETING-REJA-10-22", reply_markup=keyboard_back_to_home_lat)
        if call.data == "marketing_plan_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/MARKETING-REZHA-11-25", reply_markup=keyboard_back_to_home_cyr)
        if call.data == "5days_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/5-KUNDA-103-MILLION-SOM-DAROMAD-OLISH-10-23",
                                  reply_markup=keyboard_back_to_home_lat)
        if call.data == "5days_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/5-KUNDA-103-MILLION-S%D0%8EM-DAROMAD-OLISH-11-25",
                                  reply_markup=keyboard_back_to_home_cyr)

        if call.data == "goals_lat":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/BIZNING-MAQSAD-VA-KELAJAK-REJALAR-10-10",
                                  reply_markup=keyboard_back_to_home_lat)
        if call.data == "goals_cyr":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="https://telegra.ph/BIZNING-MA%D2%9ASAD-VA-KELAZHAK-REZHALAR-11-30",
                                  reply_markup=keyboard_back_to_home_cyr)
        if call.data == "statistics_lat":
            users_count = Users.select().count()
            #ref_1_purchases_count = Users.get(user_id=parent_id)
            purchases_count = 0
            for user in Users.select():
                purchases_count += user.purchase_count
            payed_money = 0
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="UMUMIY STATISTIKA\n"
                                  "--------------------------------------\n"
                                  f"Foydalanuvchilar soni: {users_count}\n"
                                  f"Xaridlar soni: {purchases_count}\n"
                                  f"To'langan mablag'lar: {payed_money}\n",
                                  reply_markup=keyboard_back_to_home_lat)
        if call.data == "statistics_cyr":
            users_count = Users.select().count()
            #ref_1_purchases_count = Users.get(user_id=parent_id)
            purchases_count = 0
            for user in Users.select():
                purchases_count += user.purchase_count
            payed_money = 0
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="УМУМИЙ СТАТИСТИКА\n"
                                  "--------------------------------------\n"
                                  f"Фойдаланувчилар сони: {users_count}\n"
                                  f"Харидлар сони: {purchases_count}\n"
                                  f"Тўланган маблағлар: {payed_money}\n",
                                  reply_markup=keyboard_back_to_home_cyr)
        if call.data == "ref_link_lat":
            if Users.get(user_id=call.message.chat.id).isActive is True:
                bot_name = bot.get_me().username
                ref_text = ref_link.format(bot_name, call.message.chat.id)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=ref_text, reply_markup=keyboard_back_to_home_lat)
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Referall ssilka(adres) olish uchun iltimos, \"Mahsulotlar\" blogidan o'zingizga kerakli bo'limni xarid qiling!", reply_markup=keyboard_back_to_home_lat)
        if call.data == "ref_link_cyr":
            if Users.get(user_id=call.message.chat.id).isActive is True:
                bot_name = bot.get_me().username
                ref_text = ref_link.format(bot_name, call.message.chat.id)
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text=ref_text, reply_markup=keyboard_back_to_home_cyr)
            else:
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text='Рефералл ссылка(адрес) олиш учун илтимос, "Маҳсулотлар" блогидан ўзингизга керакли бўлимни харид қилинг!', reply_markup=keyboard_back_to_home_cyr)


    # Если сообщение из инлайн-режима
    elif call.inline_message_id:
        if call.data == "products":
            bot.edit_message_text(inline_message_id=call.inline_message_id, text="Бдыщь")

# @bot.message_handler(content_types=["text"])
# def repeat_all_messages(message):
#     if message.text == "Kartangiz nomerini kiriting.":
#         bot.send_message(message.chat.id, message)
#         bot.register_next_step_handler(message, summ_to_vivod)
def send_for_vivod(message):
    chat_id = message.chat.id
    global card_number
    card_number = message.text
    msg = bot.send_message(chat_id, "Summani kiriting")
    bot.register_next_step_handler(msg, process_name_step)

def send_for_vivod_cyr(message):
    chat_id = message.chat.id
    global card_number
    card_number = message.text
    msg = bot.send_message(chat_id, "Суммани киритинг")
    bot.register_next_step_handler(msg, process_name_step_cyr)

def send_for_question(message):
    try:
        global question
        question = message.text
        bot.send_message('-1001417936117', "Savol: " + str(question))
    except Exception as e:
        bot.reply_to(message, 'oooops')

def process_name_step(message):
    try:
        chat_id = message.chat.id
        global summ_to_vivod
        summ_to_vivod = message.text
        s_t_v = int(summ_to_vivod) - int(summ_to_vivod) / 100 * 12
        print(s_t_v)
        bot.send_message('-1001070922343', "Karta nomeri: <code>" + str(card_number) + "</code>\nSumma: <code>" + str(summ_to_vivod) + "</code>\nOplataga: <code>" + str(s_t_v) + "</code>",
            parse_mode='markdown')
    except Exception as e:
        bot.reply_to(message, 'oooops')

def process_name_step_cyr(message):
    try:
        chat_id = message.chat.id
        global summ_to_vivod
        summ_to_vivod = message.text
        bot.send_message('-1001070922343', "Номер карты: <code>" + str(card_number) + "</code>\nСумма: <code>" + str(summ_to_vivod) + "</code>\nК оплате: <code>" + str(s_t_v) + "</code>",
            parse_mode='markdown')
    except Exception as e:
        bot.reply_to(message, 'oooops')

@bot.message_handler(commands=['ref'])
def get_my_ref(message):
    bot_name = bot.get_me().username
    bot.reply_to(message, text=ref_link.format(bot_name, message.chat.id))

@bot.message_handler(commands=['ref_count'])
def get_my_refs(message):
    count = Users.get_ref_count(message.chat.id)
    bot.reply_to(message, text=f'Count: {count}')

def send_percents(message):
    r_1_percent = PRODUCT_PRICE / 100 * 10
    r_2_percent = PRODUCT_PRICE / 100 * 5
    r_3_percent = PRODUCT_PRICE / 100 * 5
    r_4_percent = PRODUCT_PRICE / 100 * 3
    r_5_percent = PRODUCT_PRICE / 100 * 2
    query = Users.update(balance=Users.balance + r_1_percent).where(
        Users.user_id == Users.get(user_id=message.chat.id).ref_id_1)
    query.execute()
    query = Users.update(balance=Users.balance + r_2_percent).where(
        Users.user_id == Users.get(user_id=message.chat.id).ref_id_2)
    query.execute()
    query = Users.update(balance=Users.balance + r_3_percent).where(
        Users.user_id == Users.get(user_id=message.chat.id).ref_id_3)
    query.execute()
    query = Users.update(balance=Users.balance + r_4_percent).where(
        Users.user_id == Users.get(user_id=message.chat.id).ref_id_4)
    query.execute()
    query = Users.update(balance=Users.balance + r_5_percent).where(
        Users.user_id == Users.get(user_id=message.chat.id).ref_id_5)
    query.execute()
    set_active(message)

def set_active(message):
    query = Users.update(isActive=True).where(Users.user_id == message.chat.id)
    query.execute()

def set_purchase_count(message):
    query = Users.update(purchase_count=Users.purchase_count + 1).where(Users.user_id == message.chat.id)
    query.execute()

@bot.pre_checkout_query_handler(func=lambda query: True)
def checkout(pre_checkout_query):
    bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True, error_message="Aliens tried to steal your card's CVV, but we successfully protected your credentials,"
                                                " try to pay again in a few minutes, we need a small rest.")


@bot.message_handler(content_types=['successful_payment'])
def got_payment(message):
    if message.successful_payment.invoice_payload == 'rel_for_man_lat':
        file_data = open('files/munosabatlar_yigitlar_uchun_lat.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'rel_for_man_cyr':
        file_data = open('files/munosabatlar_yigitlar_uchun_cyr.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'rel_for_woman_lat':
        file_data = open('files/munosabatlar_ayollar_uchun_lat.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'rel_for_woman_cyr':
        file_data = open('files/munosabatlar_ayollar_uchun_cyr.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'finance_lat':
        file_data = open('files/pul_va_boylik_lat.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'finance_cyr':
        file_data = open('files/pul_va_boylik_cyr.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'health_lat':
        file_data = open('files/salomatlik_lat.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'health_cyr':
        file_data = open('files/salomatlik_cyr.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'happy_lat':
        file_data = open('files/baxt_va_omad_lat.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'happy_cyr':
        file_data = open('files/baxt_va_omad_cyr.zip', 'rb')
        bot.send_document(message.chat.id, file_data)
        send_percents(message)
        set_purchase_count(message)
    elif message.successful_payment.invoice_payload == 'upbalance':
        query = Users.update(balance=Users.balance + 5000).where(Users.user_id == message.chat.id)
        query.execute()
        set_active(message)

if __name__ == '__main__':
    bot.polling(none_stop=True, interval=0)