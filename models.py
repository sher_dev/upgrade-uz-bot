from peewee import *

db = SqliteDatabase('users.db')


class BaseModel(Model):
    class Meta:
        database = db


class Users(BaseModel):
    user_id = IntegerField(unique=True)
    balance = IntegerField(default=0)
    ref_id_1 = IntegerField(default=0)
    ref_id_2 = IntegerField(default=0)
    ref_id_3 = IntegerField(default=0)
    ref_id_4 = IntegerField(default=0)
    ref_id_5 = IntegerField(default=0)
    purchase_count = IntegerField(default=0)
    isActive = BooleanField(default=False)

    @classmethod
    def get_user(cls, user_id):
        return cls.get(user_id == user_id)

    @classmethod
    def get_grandpa(cls, user_id):
        return cls.get_user(user_id).ref_id_1

    @classmethod
    def get_ref_count(cls, user_id):
        return cls.get_user(user_id).ref

    @classmethod
    def increase_ref_count(cls, user_id):
        user = cls.get_user(user_id)
        user.ref += 1
        user.save()

    @classmethod
    def add_parents(cls, user_id, parent_id):
        user = cls.get_user(user_id)
        user.ref_id_1 = parent_id
        user.save()

    @classmethod
    def user_exists(cls, user_id):
        query = cls().select().where(cls.user_id == user_id)
        return query.exists()

    @classmethod
    def create_user(cls, user_id):
        user, created = cls.get_or_create(user_id=user_id)

